;(function ($, window, undefined) {
    window.Mobile = function() {
        var app = this;

        app.minSize = 800;
        app.test = false;

        app.Init = function() {
            $('body').append('<div id="mobile"></div>');
            app.mobile = $('#mobile');

            app.isMobile = false;
            app.isDesktop = false;
            app.site = $('html');
            app.desktop = $('#form1');

            app.ShowRequiredVersion();
            $(window).resize(function() {
                app.AdjustMenu();
                app.ShowRequiredVersion();
            });

            app.mobile.append('<div id="mobile-content"></div>');
            app.mobileContent = $('#mobile-content');

            if($('.newsSummary').length != 0 && $('.newsItemDetail').length == 0) {
                app.InitNews();
            }
            else if($('.test-template').length != 0) {
                app.InitInterior(true);
            }
            else if($('#etsMCTop').length != 0) {
                app.InitInterior(false);
            }
            else {
                app.InitHome();
            }
        };

        app.InitHome = function() {
            app.mobile.append('<div id="mobile-splash"></div>');
            app.mobileSplash = $('#mobile-splash');
            app.LoadTemplate(app.mobileSplash, 'splash');

            app.LoadTemplate(app.mobileContent, 'home', app.InitMobileHomeContent);
        };

        app.InitInterior = function(test) {
            app.test = test;
            if(test) {
                app.LoadTemplate(app.mobileContent, 'template', app.InitMobileInteriorContent);
            }
            else {
                app.LoadTemplate(app.mobileContent, 'interior', app.InitMobileInteriorContent);
            }
        };

        app.InitNews = function() {
            app.LoadTemplate(app.mobileContent, 'news', app.InitMobileNewsContent);
        };

        app.IsMobileSize = function() {
            var screenWidth = $(window).width();
            return screenWidth < app.minSize - 15;
        };

        app.AdjustMenu = function() {
            var item = $('.megaMenuBack .first');
            item.css('margin-left', $('#etslogo').offset().left + 'px');
        };

        app.ShowRequiredVersion = function() {
            if(app.IsMobileSize()) {
                if(!app.isMobile) {
                    app.site.addClass('mobile');
                    app.desktop.hide();
                    app.mobile.show();
                    app.isMobile = true;
                    app.isDesktop = false;
                }
            }
            else {
                if(!app.isDesktop) {
                    app.site.removeClass('mobile');
                    app.mobile.hide();
                    app.desktop.show();
                    app.isMobile = false;
                    app.isDesktop = true;
                }
            }
        };

        app.LoadTemplate = function(element, template, callback) {
            $.get('http://pouletfrit.ad.etsmtl.ca:8080/ETS/media/ImagesETS/Biblio/templates/' + template + '.html', function(data){
                element.prepend(data);
                if(callback){
                    callback();
                }
            });
        };

        app.InitMobileHomeContent = function() {
            $('.accordeon a.btn').click(function(e) {
                if($(this).parent().hasClass('open')) {
                    $(this).parent().removeClass('open');
                }
                else {
                    $('.accordeon').removeClass('open');
                    $(this).parent().addClass('open');
                }

                e.preventDefault();
            });

            $('input#txtSearch1').keypress(function(event) {
                if (event.which == 13) {
                    event.preventDefault();
                    $('#inputQueryPrimo1').val($(this).val());
                    lancerRecherche(1);
                }
            });

            $('input#txtSearch3').keypress(function(event) {
                if (event.which == 13) {
                    event.preventDefault();
                    $('#inputQueryPrimo3').val($(this).val());
                    lancerRecherche(3);
                }
            });

            app.InitMobileNews();

            app.LoadTemplate(app.mobileContent, 'nav', app.Render);
        };

        app.InitMobileInteriorContent = function() {
            if(!app.test) {
                var breadcrumbs = $('#etsMCTop').html();
                breadcrumbs = breadcrumbs.replace(/<(?:.|\n)*?>/gm, '');
                breadcrumbs = breadcrumbs.replace(new RegExp('&nbsp;', 'g'), '');
                $('#mobile-breadcrumbs').html(breadcrumbs);
                
                $('#mobile-page').html($('#etsMCContent').html());

                var rightColumn = $('#etsMCRight');
                var sidebarContent=0;

                if(rightColumn.find('.etsMDEPBleuPale').length != 0) {
                    var first = rightColumn.find('.etsMDEPBleuPale').html();
                    first = first.replace(new RegExp('<br>', 'g'), '');

                    $('#mobile-footer p').html(first);
                    sidebarContent++;
                }

                if(rightColumn.find('.etsMDEPOrange').length != 0) {
                    var second = rightColumn.find('.etsMDEPOrange').html();
                    second = second.replace(new RegExp('<br>', 'g'), '');
                    second = second.replace(new RegExp('<a ', 'g'), '<li><a ');
                    second = second.replace(new RegExp('</a>', 'g'), '</a></li>');

                    $('#mobile-footer ul').html(second);
                    sidebarContent++;
                }

                if(rightColumn.find('.etsMDEStandard').length != 0) {
                    $('#mobile-footer').prepend(rightColumn.find('.etsMDEStandard').html());
                    sidebarContent++;
                }
                
                if(rightColumn.find('.etsMDEPBleuGris').length != 0) {
	                rightColumn.find('.etsMDEPBleuGris').each(function () {
		                var fourth = $(this).html();
						$('#mobile-footer').prepend(fourth);
						sidebarContent++;
					});
                }
                
                if(sidebarContent == 0) $("#mobile-footer").hide();

                $('#mobile .etsBlocRetractableBouttonFerme').click(function(e) {
                    $(this).toggleClass('open');
                    $(this).next().toggle();

                    e.preventDefault();
                });

                $('#mobile a').click(function(e) {
                    var anchor = $(this).attr('href');
                    if(anchor.startsWith('#') && anchor != '#' && anchor != '#cd-primary-nav') {
                        anchor = anchor.replace('#', '');

                        if(anchor == 'top') {
                            $('#scrolling-content').scrollTop(0);
                        }
                        else {
                            var target = $('[name="' + anchor + '"]');
                            $('#scrolling-content').scrollTop($(target[1]).offset().top);
                        }

                        e.preventDefault();
                    }

                });

                $('#mobile input').keyup(function() {
                    var id = $(this).attr('id');
                    var value = $(this).val();

                    $('#' + id).val(value);
                });

                if($('.newsSummary').length != 0) {
                    $('#mobile-new-header').show();
                }
            }

            app.LoadTemplate(app.mobileContent, 'nav', app.Render);
        };

        app.InitMobileNewsContent = function() {
            var select = $('select#selNews');
            $('.CMSListMenuUL li').each(function() {
                var link = $(this).children();
                var href = link.attr('href');
                var title = link.html();

                var selected = '';
                if(link.hasClass('CMSListMenuLinkHighlighted')) {
                    selected = ' selected';
                }

                select.append('<option value="' + href + '"' + selected + '>' + title + '</option>');
            });

            select.change(function() {
                window.location.href = $(this).val();
            });

            var newsList = $('.newsSummary').html();
            $('#mobile-news-list').html(newsList);

            app.LoadTemplate(app.mobileContent, 'nav', app.Render);
        };

        app.Render = function() {
            if($('.latest-news-link').length != 0) {
                $('.latest-news').attr('href', $('.latest-news-link a').attr('href'));
            }

            var nav = $('nav.cd-nav');
            var desktopNavContainer = $('#mainMegaMenu');
            var desktopNewNavContainer = $('.megaMenuBack');

            var menu = [];
            var level = [];

            var firstLevel = $('.mmelement .cbb');
            firstLevel.each(function() {
                level = [];
                var secondLevel = $(this).children('ul');
                secondLevel.each(function() {
                    var items = [];
                    var item = null;
                    var thirdLevel = $(this).find('ul li');
                    if(thirdLevel.length > 0) {
                        thirdLevel.each(function() {
                            items.push($(this).html());
                            item = $(this);
                        });
                    }
                    var obj = {
                        'title' : $(this).find('h1').html(),
                        'items' : items,
                        'item'  : item
                    };

                    if(obj.title) {
                        level.push(obj);
                    }
                    else {
                        obj.item.find('a').each(function() {
                            var single = {
                                title : $(this)[0].outerHTML,
                                items : [],
                                item : null
                            }
                            level.push(single);
                        });
                    }
                });
                menu.push(level);
            });

            $('.cd-secondary-nav').each(function(index) {
                var element = $(this);
                var title = element.children('.title').html();
                $(menu[index]).each(function() {
                    if($(this)[0].items.length != 0) {
                        var html = '<li class="has-children">';
                        html += '<a href="#">' + $(this)[0].title + '</a>';
                        html += '<ul class="is-hidden">';
                        html += '<li class="go-back"><a href="#">' + title + '</a></li>';
                        html += '<li class="title">' + $(this)[0].title + '</li>';
                        $.each($(this)[0].items, function(i, e) {
                            html += '<li>' + e + '</li>';
                        });
                        html += '</ul>';
                        html += '</li>';
                        element.append(html);
                    }
                    else {
                        element.append('<li>' + $(this)[0].title + '</li>');
                    }
                });
            });

            desktopNavContainer.html('');
            desktopNewNavContainer.html(nav.html());

            $('#scrolling-content').css('height', ($('#mobile').height() - 50) + 'px');

            app.Menu();
            app.AdjustMenu();

            setTimeout(function() {
                app.mobileContent.show();
                if(app.mobileSplash) {
                    app.mobileSplash.hide();
                }
            }, 2000);
        };

        app.InitMobileNews = function() {
            var $news = $('ul.news');
            $('.etsANewsTitle a').each(function() {
                var $this = $(this);

                var title = $this.html();
                var href = $this.attr('href');
                var subtitle = $this.parent().parent().next().html().trim();

                $news.append('<li><a href="' + href + '">' + title + '</a><span>' + subtitle + '</span></li>');
            });
        };

        app.Menu = function() {
            //if you change this breakpoint in the style.css file (or _layout.scss if you use SASS), don't forget to update this value as well
            var MqL = 1170;
            //move nav element position according to window width
            moveNavigation();
            $(window).on('resize', function(){
                (!window.requestAnimationFrame) ? setTimeout(moveNavigation, 300) : window.requestAnimationFrame(moveNavigation);
            });

            //mobile - open lateral menu clicking on the menu icon
            $('.cd-nav-trigger').on('click', function(event){
                event.preventDefault();
                if( $('.cd-main-content').hasClass('nav-is-visible') ) {
                    closeNav();
                    $('.cd-overlay').removeClass('is-visible');
                } else {
                    $(this).addClass('nav-is-visible');
                    $('.cd-primary-nav').addClass('nav-is-visible');
                    $('.cd-main-header').addClass('nav-is-visible');
                    $('.cd-main-content').addClass('nav-is-visible').one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function(){
                        $('body').addClass('overflow-hidden');
                    });
                    toggleSearch('close');
                    $('.cd-overlay').addClass('is-visible');
                }
            });

            //open search form
            $('.cd-search-trigger').on('click', function(event){
                event.preventDefault();
                toggleSearch();
                closeNav();
            });

            //close lateral menu on mobile
            $('.cd-overlay').on('swiperight', function(){
                if($('.cd-primary-nav').hasClass('nav-is-visible')) {
                    closeNav();
                    $('.cd-overlay').removeClass('is-visible');
                }
            });
            $('.nav-on-left .cd-overlay').on('swipeleft', function(){
                if($('.cd-primary-nav').hasClass('nav-is-visible')) {
                    closeNav();
                    $('.cd-overlay').removeClass('is-visible');
                }
            });
            $('.cd-overlay').on('click', function(){
                closeNav();
                toggleSearch('close')
                $('.cd-overlay').removeClass('is-visible');
            });


            //prevent default clicking on direct children of .cd-primary-nav
            $('.cd-primary-nav').children('.has-children').children('a').on('click', function(event){
                event.preventDefault();
            });
            //open submenu
            $('.has-children').children('a').on('click', function(event){
                if( !checkWindowWidth() ) event.preventDefault();
                var selected = $(this);
                if( selected.next('ul').hasClass('is-hidden') ) {
                    //desktop version only
                    selected.addClass('selected').next('ul').removeClass('is-hidden').end().parent('.has-children').parent('ul').addClass('moves-out');
                    selected.parent('.has-children').siblings('.has-children').children('ul').addClass('is-hidden').end().children('a').removeClass('selected');
                    $('.cd-overlay').addClass('is-visible');
                } else {
                    selected.removeClass('selected').next('ul').addClass('is-hidden').end().parent('.has-children').parent('ul').removeClass('moves-out');
                    $('.cd-overlay').removeClass('is-visible');
                }
                toggleSearch('close');
            });

            //submenu items - go back link
            $('.go-back').on('click', function(event){
                event.preventDefault();
                $(this).parent('ul').addClass('is-hidden').parent('.has-children').parent('ul').removeClass('moves-out');
            });

            function closeNav() {
                $('.cd-nav-trigger').removeClass('nav-is-visible');
                $('.cd-main-header').removeClass('nav-is-visible');
                $('.cd-primary-nav').removeClass('nav-is-visible');
                $('.has-children ul').addClass('is-hidden');
                $('.has-children a').removeClass('selected');
                $('.moves-out').removeClass('moves-out');
                $('.cd-main-content').removeClass('nav-is-visible').one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function(){
                    $('body').removeClass('overflow-hidden');
                });
            }

            function toggleSearch(type) {
                if(type=="close") {
                    //close serach
                    $('.cd-search').removeClass('is-visible');
                    $('.cd-search-trigger').removeClass('search-is-visible');
                    $('.cd-overlay').removeClass('search-is-visible');
                } else {
                    //toggle search visibility
                    $('.cd-search').toggleClass('is-visible');
                    $('.cd-search-trigger').toggleClass('search-is-visible');
                    $('.cd-overlay').toggleClass('search-is-visible');
                    if($(window).width() > MqL && $('.cd-search').hasClass('is-visible')) $('.cd-search').find('input[type="search"]').focus();
                    ($('.cd-search').hasClass('is-visible')) ? $('.cd-overlay').addClass('is-visible') : $('.cd-overlay').removeClass('is-visible') ;
                }
            }

            function checkWindowWidth() {
                //check window width (scrollbar included)
                var e = window,
                    a = 'inner';
                if (!('innerWidth' in window )) {
                    a = 'client';
                    e = document.documentElement || document.body;
                }
                if ( e[ a+'Width' ] >= MqL ) {
                    return true;
                } else {
                    return false;
                }
            }

            function moveNavigation(){
                var navigation = $('.cd-nav');
                var desktop = checkWindowWidth();
                if ( desktop ) {
                    navigation.detach();
                    navigation.insertBefore('.cd-header-buttons');
                } else {
                    navigation.detach();
                    navigation.insertAfter('.cd-main-content');
                }
            }
        };

    };

    window.mobile = new Mobile();

    $(document).ready(function() {
        var isInIframe = window.location != window.parent.location;
        if (isInIframe) {
            $('#form1').show();
        }
        else {
            mobile.Init();
        }
    });
}(jQuery, window));

